<?php

namespace Application\Welcome\Domain\Model;

use Symfony\Component\Uid\UuidV4;

class Tarea
{

    private $id_tarea;
    private $tarea;
    private $estado = 0;

    /**
     * @param string $tarea
     */
    public function __construct(string $tarea)
    {
        $this->id_tarea = UuidV4::v4()->toRfc4122();
        $this->tarea = $tarea;
    }


    public function changeEstado(int $estado)
    {
        $this->estado = $estado;
    }

    /**
     * @return string
     */
    public function getIdTarea(): string
    {
        return $this->id_tarea;
    }

    /**
     * @return string
     */
    public function getTarea(): string
    {
        return $this->tarea;
    }

    /**
     * @return int
     */
    public function getEstado(): int
    {
        return $this->estado;
    }


}