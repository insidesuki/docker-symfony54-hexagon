<?php

namespace Application\Welcome\Infrastructure\Storage\Doctrine\Repository;

use Application\Common\Infrastructure\Storage\Doctrine\AbstractDoctrineRepository;
use Application\Welcome\Domain\Model\Tarea;
use Application\Welcome\Domain\Repository\TareaRepository;

class TareaDoctrineRepository extends AbstractDoctrineRepository implements TareaRepository
{

    /**
     * @inheritDoc
     */
    protected static function entityClass(): string
    {
        return Tarea::class;
    }

    public function findTareaById(string $id): ?Tarea
    {
       return $this->objectRepository->find($id);
    }

    public function save(Tarea $tarea): Tarea
    {
        $this->saveEntity($tarea);
        return $tarea;
    }
}