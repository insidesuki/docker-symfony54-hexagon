<?php

namespace Application\Common\Infrastructure\Storage\Doctrine;

use Doctrine\DBAL\Connection;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Persistence\ObjectManager;

abstract class AbstractDoctrineRepository
{
    protected $objectRepository;
    /**
     * @var ManagerRegistry
     */
    private $managerRegistry;

    public function __construct(
        ManagerRegistry $managerRegistry
    )
    {
        $this->managerRegistry = $managerRegistry;
        $this->objectRepository = $this->getEntityManager()->getRepository($this->entityClass());
    }


    /**
     * @return ObjectManager
     */
    protected function getEntityManager(): ObjectManager
    {
        $entityManager = $this->managerRegistry->getManager();

        if ($entityManager->isOpen()) {
            return $entityManager;
        }

        return $this->managerRegistry->resetManager();
    }

    /**
     * @return string
     */
    abstract protected static function entityClass(): string;


    /**
     * @param object $entity
     */
    protected function saveEntity(object $entity,bool $flush = true): void
    {
        $this->getEntityManager()->persist($entity);
        if($flush){
            $this->getEntityManager()->flush();
        }
        
    }

    /**
     * @param object $entity
     */
    protected function removeEntity(object $entity,bool $flush = true): void
    {
        $this->getEntityManager()->remove($entity);
         if($flush){
            $this->getEntityManager()->flush();
        }
      
    }

}
