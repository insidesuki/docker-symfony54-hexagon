<?php

namespace Application\Common\Domain\Exception;

use LogicException;

class EntityAlreadyExistsException extends LogicException
{
    public function __construct($message)
    {
        parent::__construct(sprintf('Entity:"%s", already exists!!', $message));
    }

}