<?php

namespace Application\Common\Domain\Exception;

use LogicException;

class EntityDoesNotExistsException extends LogicException
{

    public function __construct($message)
    {
        parent::__construct(sprintf('Entity:"%s", does not exists!!', $message));
    }

}