<?php

namespace Application\Common\User\Infrastructure\Storage\Doctrine\Repository;

use Application\Common\Infrastructure\Storage\Doctrine\AbstractDoctrineRepository;
use Application\Common\User\Domain\Model\User;
use Application\Common\User\Domain\Repository\UserRepository;

class UserDoctrineRepository extends AbstractDoctrineRepository implements UserRepository
{

    /**
     * @inheritDoc
     */
    protected static function entityClass(): string
    {
        return User::class;
    }

    /**
     * @param string $email
     * @return User|null
     */
    public function findByEmail(string $email): ?User
    {
        return $this->objectRepository->findOneBy(['email' => $email]);
    }

    /**
     * @param User $user
     * @return User
     */
    public function save(User $user): User
    {
        $this->save($user);
        return $user;

    }
}