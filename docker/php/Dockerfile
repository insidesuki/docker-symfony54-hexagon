FROM php:8.1-fpm

ARG UID
EXPOSE $UID

RUN adduser -u ${UID} --disabled-password --gecos "" app_oper
RUN mkdir /home/app_oper/.ssh
RUN chown -R app_oper:app_oper /home/app_oper/
RUN echo "StrictHostKeyChecking no" >> /home/app_oper/.ssh/config
RUN echo "alias sym=/application/bin/console" >> /home/app_oper/.bashrc
RUN echo "alias unitest=/application/vendor/bin/phpunit" >> /home/app_oper/.bashrc

COPY ./php.ini /usr/local/etc/php/php.ini

RUN apt-get update \
    && apt-get install -y git acl openssl openssh-client wget zip vim libssh-dev \
    && apt-get install -y libpng-dev zlib1g-dev libzip-dev libxml2-dev libicu-dev \
    && docker-php-ext-install intl pdo pdo_mysql zip gd soap bcmath sockets \
    && pecl install xdebug \
    && docker-php-ext-enable --ini-name 05-opcache.ini opcache xdebug

RUN curl https://getcomposer.org/composer.phar -o /usr/bin/composer && chmod +x /usr/bin/composer
RUN composer self-update

# Install PHP-CS-FIXER
RUN wget https://cs.symfony.com/download/php-cs-fixer-v3.phar -O php-cs-fixer
RUN chmod a+x php-cs-fixer
RUN mv php-cs-fixer /usr/local/bin/php-cs-fixer

## Install Symfony binary
USER app_oper
RUN wget https://get.symfony.com/cli/installer -O - | bash
USER root
RUN mv /home/app_oper/.symfony/bin/symfony /usr/local/bin/symfony

RUN mkdir -p /application

# Config XDEBUG
COPY ./xdebug-linux.ini /usr/local/etc/php/conf.d/xdebug.ini

WORKDIR /application
