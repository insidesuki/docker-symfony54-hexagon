**DOCKER SYMFONY54-HEXAGON**
==============================

Docker containers for Symfony 5.4 (PHP-8.1-FPM,NginX,MySQL 8.0), HEXAGON Skeleton.

*Remember:*
- Make sure you have docker installed
@url: https://docs.docker.com/engine/install/ubuntu/*

- Dont run docker as a root, instead add docker group to your current user.
@url:https://www.configserverfirewall.com/ubuntu-linux/add-user-to-docker-group-ubuntu/

- Install docker compose.
@url: https://docs.docker.com/compose/install/

**Containers**
==============================
- Php-8.1-FPM
- MySQL 8.0
- Nginx (latest version)


**To run**
==============================
$ make build

$ make run

$ make prepare (composer install)

$ make ssh-docker (ssh to backend)

**Services**
==============================
- Webserver http://localhost:500
- PHP-FPM: 9000
- MySQL: 3311
- Composer
- xDebug

**Folder Structure**
==============================
- src: only Symfony Kernel.
- app: hexagon-architecture of your app
- app/Common: Shared logic, between context. The Security componente lives here.
- app/{context}/Infrastructure/Storage/Doctrine/Mappgins: here lives the orm mapper xml file.
- config: Symfony config files and add ENV folder, to store all .env
- config/orm/migrations: Doctrine migrations files.